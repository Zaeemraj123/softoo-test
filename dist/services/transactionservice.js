"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrnsactionService = exports.processTransactions = void 0;
const fs = require("fs");
const response_model_1 = require("../models/response.model");
const processTransactions = async (sku, currentQuantity) => {
    try {
        const transactionsData = JSON.parse(fs.readFileSync('./src/json/transactions.json', 'utf8'));
        transactionsData.forEach((transaction) => {
            if (transaction.sku === sku) {
                if (transaction.type === 'order') {
                    currentQuantity += transaction.qty;
                }
                else if (transaction.type === 'refund') {
                    currentQuantity -= transaction.qty;
                }
            }
        });
        return Promise.resolve({ sku, qty: currentQuantity });
    }
    catch (error) {
        return Promise.reject(error);
    }
};
exports.processTransactions = processTransactions;
class TrnsactionService {
    constructor(sku_, currentQuantity_) {
        this.sku_ = sku_;
        this.currentQuantity_ = currentQuantity_;
        this.sku = null;
        this.currentQuantity = 0;
        this.sku = sku_;
        this.currentQuantity = currentQuantity_;
    }
    getCurrentTransactions() {
        let responseModel = new response_model_1.ResponseModel();
        try {
            const transactionData = this.getMockData();
            if (transactionData.length) {
                transactionData.forEach((transaction) => {
                    if (transaction.sku === this.sku) {
                        if (transaction.type === 'order') {
                            this.currentQuantity += transaction.qty;
                        }
                        else if (transaction.type === 'refund') {
                            this.currentQuantity -= transaction.qty;
                        }
                    }
                });
                if (this.currentQuantity) {
                    responseModel.isSuccess = true;
                    responseModel.data.qty = this.currentQuantity;
                }
            }
            return responseModel;
        }
        catch (error) {
            return responseModel;
        }
    }
    getMockData() {
        let TransactionData = [];
        try {
            TransactionData = JSON.parse(fs.readFileSync('./src/json/transactions.json', 'utf8'));
            return TransactionData || [];
        }
        catch (error) {
            return TransactionData;
        }
    }
}
exports.TrnsactionService = TrnsactionService;
