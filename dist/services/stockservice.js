"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Stocks = void 0;
const response_model_1 = require("../../src/models/response.model");
const fs = require("fs");
class Stocks {
    constructor(sku) {
        this.sku = null;
        this.sku = sku;
    }
    getCurrentStock() {
        let responseModel = new response_model_1.ResponseModel();
        try {
            const stockData = this.getMockData();
            if (stockData.length) {
                const startingStock = stockData.find((item) => item.sku === this.sku);
                if (startingStock) {
                    responseModel.isSuccess = true;
                    responseModel.data = startingStock;
                }
            }
            return responseModel;
        }
        catch (error) {
            return responseModel;
        }
    }
    getMockData() {
        let stockData = [];
        try {
            stockData = JSON.parse(fs.readFileSync('./src/json/stock.json', 'utf8'));
            return stockData || [];
        }
        catch (error) {
            return stockData;
        }
    }
}
exports.Stocks = Stocks;
