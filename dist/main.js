"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MainClass = void 0;
const transactionService_1 = require("./services/transactionService");
const stockService_1 = require("./services/stockService");
const readline = require("readline");
(async () => {
    new MainClass().checkStockRatesAndTransactions();
    const sku = 'LTV719449/39/39';
})();
class MainClass {
    checkStockRatesAndTransactions() {
        let stockResponse;
        let transactionResponse;
        const rl = readline.createInterface({ input: process.stdin, output: process.stdout });
        rl.question('Enter SKU: ', async (userInput) => {
            if (userInput) {
                stockResponse = new stockService_1.Stocks('LTV719449/39/39').getCurrentStock();
                if (stockResponse.isSuccess) {
                    console.log(`Current stock for SKU ${userInput}: ${stockResponse.data.stock}`);
                }
            }
            if (userInput) {
                transactionResponse = new transactionService_1.TrnsactionService('LTV719449/39/39', stockResponse.data.stock).getCurrentTransactions();
                if (stockResponse.isSuccess) {
                    console.log(`Updated stock for SKU ${userInput}: ${stockResponse.data.stock}`);
                }
            }
            rl.close();
        });
    }
}
exports.MainClass = MainClass;
