"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionData = void 0;
class TransactionData {
    constructor() {
        this.sku = "";
        this.type = "";
        this.qty = 0;
    }
}
exports.TransactionData = TransactionData;
