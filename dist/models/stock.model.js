"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StockData = void 0;
class StockData {
    constructor() {
        this.sku = '';
        this.stock = 0;
    }
}
exports.StockData = StockData;
