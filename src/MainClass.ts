import { ResponseModel } from './models/response.model';
import { StockData } from './models/stock.model';
import { TransactionData } from './models/transaction.model';
import { TrnsactionService } from './services/transaction/transactionService';
import { Stocks } from './services/stock/stockService';
import * as readline from 'readline';



export class MainClass {

    checkStockRatesAndTransactions() {
        let stockResponse: ResponseModel<StockData>;
        let transactionResponse: ResponseModel<TransactionData>;
        const rl = readline.createInterface({ input: process.stdin, output: process.stdout });

        rl.question('Enter SKU: ', async (userInput: string) => {

            if (userInput) {
                stockResponse = new Stocks(userInput).getCurrentStock();  
                if (stockResponse.isSuccess) {
                    console.log(`Current stock for SKU ${userInput}: ${stockResponse.data.stock}`);
                }
                else {
                    console.log(`Current stock for SKU ${userInput}: 0`);

                }
            } 

            if (stockResponse.isSuccess) {
                transactionResponse = new TrnsactionService(userInput, stockResponse.data.stock).getCurrentTransactions();
                if (transactionResponse.isSuccess) {
                    console.log(`Updated stock for SKU ${userInput}: ${transactionResponse.data.qty}`);
                }
                else {
                    console.log(`Updated stock for SKU ${userInput}: 0`);

                }
            }
            else {
                console.log(`No Transactions`);

            }

            rl.close();
        });
    }

}