export class ResponseModel<T> {
    isSuccess: boolean = false;
    data: T;
}