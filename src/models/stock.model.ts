export class StockData {
    public sku:string = '';
    public stock: number = 0;
}