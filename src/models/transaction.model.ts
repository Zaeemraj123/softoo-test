export class TransactionData {

    public sku: string = "";
    public type: string = "";
    public qty: number = 0;
}