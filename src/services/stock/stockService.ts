import {  StockData  } from '../../models/stock.model';
import {  ResponseModel  } from '../../models/response.model';
import * as fs from 'fs';

export class Stocks {
  public sku: string = null;
  constructor(sku: string) {
    this.sku = sku;
  }

  
  getCurrentStock(): ResponseModel<StockData> {
    let responseModel: ResponseModel<StockData> = new ResponseModel()
    try {
      const stockData: StockData[] = this.getMockData();
      if (stockData.length) {
        const startingStock = stockData.find((item: StockData) => item.sku === this.sku);
        if (startingStock) {
          responseModel.isSuccess = true;
          responseModel.data = startingStock;
        }
        else{
          responseModel.isSuccess = false;
        }
      }
      return responseModel
    } catch (error) {
      return responseModel;
    }
  }



  private getMockData(): StockData[] {
    let stockData: StockData[] = [];
    try {
      stockData = JSON.parse(fs.readFileSync('./src/mockData/stock.json', 'utf8'));
      return stockData || [];
    } catch (error) {
      return stockData;
    }
  } 


}
