import { Stocks } from './stockService';
import { StockData } from '../../models/stock.model';

describe('Stocks', () => {
  let stocks: Stocks;

  beforeEach(() => {
    stocks = new Stocks('LTV719449/39/39');
  });

  it('should be created', () => {
    expect(stocks).toBeTruthy();
  });

  it('should return a ResponseModel with isSuccess true if stock exists', () => {
    const response = stocks.getCurrentStock();
    expect(response.isSuccess).toBeTruthy();
  });

  it('should return a ResponseModel with isSuccess false if stock does not exist', () => {
    jest.spyOn(stocks, 'getMockData' as any).mockReturnValue([]);
    
    const response = stocks.getCurrentStock();
    expect(response.isSuccess).toBeFalsy();
  });

  it('should return the correct stock data', () => {
    jest.spyOn(stocks, 'getMockData' as any).mockReturnValue([
      { sku: 'LTV719449/39/39', stock: 10 },
    ]);

    const response = stocks.getCurrentStock();
    expect(response.data.stock).toBe(10);
  });


});
