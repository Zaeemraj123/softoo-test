import { TrnsactionService } from './transactionservice'; 
import { TransactionData } from '../../models/transaction.model';

describe('TransactionService', () => {
  let transactionService: TrnsactionService;

  beforeEach(() => {
    transactionService = new TrnsactionService('LTV719449/39/39', 0);
  });

  it('should be created', () => {
    expect(transactionService).toBeTruthy();
  });

  it('should return a ResponseModel with isSuccess true if transactions exist', () => {
    const response = transactionService.getCurrentTransactions();
    expect(response.isSuccess).toBeTruthy();
  });

  it('should update currentQuantity correctly for order transactions', () => {
    jest.spyOn(transactionService, 'getMockData' as any).mockReturnValue([
      { sku: 'LTV719449/39/39', type: 'order', qty: 5 },
    ]);

    const response = transactionService.getCurrentTransactions();
    expect(response.data.qty).toBe(5);
  });

  it('should update currentQuantity correctly for refund transactions', () => {
    jest.spyOn(transactionService, 'getMockData' as any).mockReturnValue([
      { sku: 'LTV719449/39/39', type: 'refund', qty: 3 },
    ]);

    const response = transactionService.getCurrentTransactions();
    expect(response.data.qty).toBe(-3);
  });


});
