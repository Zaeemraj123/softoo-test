import * as fs from 'fs';
import { ResponseModel } from '../../models/response.model';
import { TransactionData } from '../../models/transaction.model';


export class TrnsactionService {
  public sku: string = null;
  public currentQuantity: number = 0;
  constructor(public sku_: string,public currentQuantity_: number) {
    this.sku = sku_;
    this.currentQuantity = currentQuantity_;
  }

  
  getCurrentTransactions(): ResponseModel<TransactionData> {
    let responseModel: ResponseModel<TransactionData> = new ResponseModel()
    try {
      const transactionData: TransactionData[] = this.getMockData();
      if (transactionData.length) {
        transactionData.forEach((transaction: any) => {
          if (transaction.sku === this.sku) {
            if (transaction.type === 'order') {
              this.currentQuantity += transaction.qty;
            } else if (transaction.type === 'refund') {
              this.currentQuantity -= transaction.qty;
            }
          }
        });
        if (this.currentQuantity) { 
          responseModel.isSuccess = true;
          responseModel.data = new TransactionData();
          responseModel.data.qty = this.currentQuantity; 
           
        }
        else{
          responseModel.isSuccess = false;

        }
      }
      return responseModel;
    } catch (error) {
      return responseModel;
    }
  }



  private getMockData(): TransactionData[] {
    let TransactionData: TransactionData[] = [];
    try {
      TransactionData = JSON.parse(fs.readFileSync('./src/mockData/transactions.json', 'utf8'));
      return TransactionData || [];
    } catch (error) {
      return TransactionData;
    }
  }


}

